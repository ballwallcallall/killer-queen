﻿using UnityEngine;
using System.Collections;

public enum Team
{
    RedTeam,
    BlueTeam,
    Spectator
}

public class SnailPost : MonoBehaviour {
    public Team theTeam;
    string SnailTag = "Snail";
    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(SnailTag))
        {
            GameController.WinGame(theTeam, transform.position);
        }
    }
}
