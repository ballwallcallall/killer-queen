﻿using UnityEngine;
using System.Collections;

public class RollingBall : MonoBehaviour {

    new private Rigidbody2D rigidbody;
    public float hardPush = 5f; 
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D (Collision2D other)
    {
        Vector3 v = (transform.position - other.transform.position);
        rigidbody.AddForce(v * hardPush);
       // transform.Rotate()
    }
}
