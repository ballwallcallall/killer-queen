﻿using UnityEngine;
using System.Collections;

public class ControllerScript : MonoBehaviour {

    public float MaxSpeed = 10f;
    public float MaxJumpSpeed = 2f;
    public float JumpForce = 700;
    public float ContiniousJumpForce = 10f;
    bool FacingRight = true;
    public int TimesJumped = 0;

    public bool Grounded = false;   
    public bool ContiniousJump = false;
    public Transform groundCheck;
    float GroundRadius = 0.5f;
    public LayerMask WhatIsPlatform;

    public int CalledTimes = 0;

    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if(!(rb.velocity.y>0))
            Grounded = Physics2D.OverlapCircle(groundCheck.position, GroundRadius, WhatIsPlatform);
        //true means we are on the ground.

        if (Grounded)
        {
            ContiniousJump = false;
        }

        if (Grounded && Input.GetButtonDown("Jump"))
        {
            CalledTimes++;
            Grounded = false;
            rb.AddForce(new Vector2(0, JumpForce));
            //rb.velocity = new Vector2(0, JumpForce);
            ContiniousJump = true;

        }

        if (ContiniousJump&&rb.velocity.y>0)
        {
            float jump = Input.GetAxis("Jump");
            //rb.velocity = new Vector2(0, jump * MaxJumpSpeed);
            if (jump == 1)
            {
                rb.velocity += new Vector2(0, -Physics2D.gravity.y * 0.004f);
            }
            else
            {

                ContiniousJump = false;
            }
        }

        float move = Input.GetAxis("Horizontal");

        rb.velocity = new Vector2(move * MaxSpeed, rb.velocity.y);

        
        if(move > 0 && !FacingRight)
        {
            Flip();
        }
        else if(move < 0 && FacingRight)
        {
            Flip();
        }
    }

    void Flip()
    {
        FacingRight = !FacingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

}
