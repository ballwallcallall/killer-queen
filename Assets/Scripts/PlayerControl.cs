﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	[HideInInspector]
	public bool canJump = false;				// Condition for whether the player should jump.


	public float xMoveForce = 365f;			// Amount of force added to move the player left and right.
	public float xMaxSpeed = 5f;				// The fastest the player can travel in the x axis.
	public AudioClip[] jumpClips;			// Array of clips for when the player jumps.
	public float jumpForce = 1000f;			// Amount of force added when the player jumps.
	public AudioClip[] taunts;				// Array of clips for when the player taunts.
	public float tauntProbability = 50f;	// Chance of a taunt happening.
	public float tauntDelay = 1f;			// Delay for when the taunt should happen.


	private int tauntIndex;					// The index of the taunts array indicating the most recent taunt.
	private Transform groundCheck;			// A position marking where to check if the player is grounded.
	private bool grounded = false;			// Whether or not the player is grounded.
	private Animator anim;					// Reference to the player's animator component

    private Rigidbody2D theRigidBody;
    public float yMaxSpeed;

    private Vector3 GroundLevel;
    public float scale = 100;

    private Transform thisTransform;

    public float maxYDifference = 10;
    private bool firstTimeJump = true;
    private bool firstTimeEnterJump = true;
    private float OrigGravScale;
    public float JumpedGravScale = 0.5f;
    public float stopThreshold = 1f;
    public float WaitForSeconds = 1f;
    public bool FlyingUnit = false;
    public float FlyingForce = 600;
	void Awake()
	{
		// Setting up references.
		groundCheck = transform.FindChild("groundCheck");
        theRigidBody = GetComponent<Rigidbody2D>();
        thisTransform = this.transform;
	}

    private WaitForSeconds WaitForSecondsToStopGravModi;
    void Start()
    {
        OrigGravScale = theRigidBody.gravityScale;
        WaitForSecondsToStopGravModi = new WaitForSeconds(WaitForSeconds);
    }


    void Update()
    {
        if (FlyingUnit)
        {
            if(Input.GetButtonDown("Jump"))
            {
                theRigidBody.AddForce(Vector2.up * FlyingForce);
            }
        }
    }
    void SetDownwardMotion()
    {
        firstTimeEnterJump = false;
        theRigidBody.gravityScale = OrigGravScale;
    }
    void FixedUpdate()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Platforms Layer")); 
        // Cache the horizontal input.
        float x = Input.GetAxis("Horizontal") * Time.deltaTime * scale;
        float y = 0;

        if (!FlyingUnit)
        {
            if (Input.GetButton("Jump") && firstTimeJump)
            {
                y = jumpForce;
                firstTimeJump = false;
                firstTimeEnterJump = true;
                StartCoroutine(StopGravModifier());
            }
            else if (Input.GetButton("Jump") && firstTimeEnterJump)
            {
                theRigidBody.gravityScale = JumpedGravScale;
            }
            else
            {
                SetDownwardMotion();
            }
        }

        

        if(theRigidBody.velocity.y < 0)
        {
            SetDownwardMotion();
        }
        // The Speed animator parameter is set to the absolute value of the horizontal input.
        //anim.SetFloat("Speed", Mathf.Abs(h));

        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        if (x * theRigidBody.velocity.x < xMaxSpeed)
            theRigidBody.AddForce(Vector2.right * x * xMoveForce);

        if (FlyingUnit) { 
        if (y * theRigidBody.velocity.y < yMaxSpeed && canJump)
            theRigidBody.AddForce( Vector2.up * y);
        }
        else
        {
            theRigidBody.AddForce(Vector2.up * y);
        }

        // If the player's horizontal velocity is greater than the maxSpeed...
        if (Mathf.Abs(theRigidBody.velocity.x) > xMaxSpeed)
            theRigidBody.velocity = new Vector2(Mathf.Sign(theRigidBody.velocity.x) * xMaxSpeed, theRigidBody.velocity.y);
        if (Mathf.Abs(theRigidBody.velocity.y) > yMaxSpeed)
        {
            theRigidBody.velocity = new Vector2(theRigidBody.velocity.x, Mathf.Sign(theRigidBody.velocity.y) * yMaxSpeed);
            canJump = false;
            SetDownwardMotion();
        }
        
        // If the input is moving the player right and the player is facing left...
        if (x > 0 && !facingRight)
            // ... flip the player.
            Flip();
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (x < 0 && facingRight)
            // ... flip the player.
            Flip();

        // If the player should jump...
        if (grounded)
        {
            firstTimeJump = true;
            canJump = true;
            GroundLevel = thisTransform.position;
        }
        else
        {
            //if (Mathf.Abs(GroundLevel.y - thisTransform.position.y) > maxYDifference)
            //    canJump = false;
            //Debug.Log("Current diff is " + Mathf.Abs(GroundLevel.y - thisTransform.position.y));
        }
    }

    IEnumerator StopGravModifier()
    {
        yield return WaitForSecondsToStopGravModi;
        SetDownwardMotion();
        StopAllCoroutines();
    }


    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
		transform.localScale = theScale;
	}


	public IEnumerator Taunt()
	{
		// Check the random chance of taunting.
		float tauntChance = Random.Range(0f, 100f);
		if(tauntChance > tauntProbability)
		{
			// Wait for tauntDelay number of seconds.
			yield return new WaitForSeconds(tauntDelay);

			// If there is no clip currently playing.
			if(!GetComponent<AudioSource>().isPlaying)
			{
				// Choose a random, but different taunt.
				tauntIndex = TauntRandom();

				// Play the new taunt.
				GetComponent<AudioSource>().clip = taunts[tauntIndex];
				GetComponent<AudioSource>().Play();
			}
		}
	}


	int TauntRandom()
	{
		// Choose a random index of the taunts array.
		int i = Random.Range(0, taunts.Length);

		// If it's the same as the previous taunt...
		if(i == tauntIndex)
			// ... try another random taunt.
			return TauntRandom();
		else
			// Otherwise return this index.
			return i;
	}
}
